describe("Find element startegy for Android", () => {
  it("Using Accessibility label", async () => {
    // Find the App element
    const appElement = await $("~App");
    // Click the element
    await appElement.click();
    // Assert it
    const actionBarElement = await $("~Action Bar");
    await expect(actionBarElement).toBeExisting();
  });

  it("Using Class name", async () => {
    // Find the App element
    const appElement = await $("android.widget.TextView");

    await expect(appElement).toHaveText("API Demos");
  });

  it("Using xpath", async () => {
    // Example: //tagname[@attributename=value]
    const appElement = await $('//android.widget.TextView[@text="App"]');
    await appElement.click();

    await $("~Alert Dialogs").click();
    await $("id=io.appium.android.apis:id/select_button").click();

    await $('//android.widget.TextView[@text="Command one"]').click();
    await expect($("android.widget.TextView")).toHaveText(
      "You selected: 0 , Command one"
    );
  });

  it("Using Android UI Selector", async () => {
    // Example: //tagname[@attributename=value]
    const appElement = await $('android=new UiSelector().textContains("App")');
    await appElement.click();
  });

  it.only("Package and Activity", async () => {
    // Confirm the Start Activity
    await driver.startActivity(
      "io.appium.android.apis",
      "io.appium.android.apis.app.AlertDialogSamples"
    );

    await driver.pause(3000);

    // Assert the element present
    await expect($("~List dialog")).toExist();
  });
});
